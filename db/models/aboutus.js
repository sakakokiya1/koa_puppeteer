const seq = require('../connection/mysql_connect'),
      { STRING, TEXT, INT } = require('../../config/db_type_config');

const AboutUs = seq.define('aboutus', {
    aid: {
        comment: 'aboutus ID',
        type: INT,
        allowNull: false,
        unique: true
    },
    posterUrl: {
        comment: 'poster',
        type: STRING,
        allowNull: false
    },
    title: {
        comment: 'title',
        type: STRING,
        allowNull: false
    },
    name: {
        comment: 'name',
        type: STRING,
        allowNull: false
    },
    intro: {
        comment: 'introduction',
        type: TEXT,
        allowNull: false
    },
    posterImgKey: {
        comment: 'qiniu poster image key',
        type: STRING,
        allowNull: false
    }
});

module.exports = AboutUs;