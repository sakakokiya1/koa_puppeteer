const seq = require('../connection/mysql_connect'),
      { STRING, INT } = require('../../config/db_type_config');

const Collections = seq.define('collection', {
    cid: {
        comment: 'collection ID',
        type: INT,
        allowNull: false,
        unique: true
    },
    title: {
        comment: 'collection title',
        type: STRING,
        allowNull: false,
    },
    info: {
        comment: 'collection information',
        type: STRING,
        allowNull: false,
    },
    qqQunLink: {
        comment: 'the link to open QQ communication',
        type: STRING,
        allowNull: false,
    },
    posterUrl: {
        comment: 'poster Image url',
        type: STRING,
        allowNull: false,
    },
    courseIdList: {
        comment: 'this collection for containing course IDs',
        type: STRING,
        allowNull: false
    },
    posterKey: {
        comment: 'qiniu poster image name',
        type: STRING,
        allowNull: false
    },
    status: {
        comment: 'collection status',
        type: INT, // 类型
        defaultValue: 1, // 默认值
        allowNull: false, // 是否允许为空
    }
});

module.exports = Collections