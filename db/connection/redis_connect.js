const redis = require('redis'),
      { REDIS_CONF } = require('../../config/db_config');

const red = redis.createClient(REDIS_CONF);

red.on('error', (err) => {
    console.error('Redis error: ' + err);
});

module.exports = red;