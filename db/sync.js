// 同步数据库
const seq = require('./connection/mysql_connect');

// 引入模型全部模型
require('./models')

// 同步数据库时 才执行的代码
seq.authenticate().then(res => {
    console.log('MySQL server is connected completely.');
}).catch(error => {
    console.log('MySQL server is filed to be connected. Error information is below ' + error);
});

seq.sync({
    // force: true, // 注释掉不会同步其他表
}).then(() => {
    console.log('The tabel has been synchronised int odatabase successfully');
    // 关闭同步进程
    process.exit();
})