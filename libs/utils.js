const cp = require('child_process'),
      Qiniu = require('qiniu'),
      crypto = require('crypto'), // node 自带加密模块
      { resolve } = require('path'),
      { nanoid } = require('nanoid'),
      { qiniu, cryptoSecret } = require('../config/config')

function startProcess(options) {
    const script = resolve(__dirname, '../crawlers/' + options.file), // 找到子进程
            child = cp.fork(script, []); // 执行脚本

    // 子进程是否被调用了
    let invoked = false;

    // 如果进程发送了信息(send信息) 则在message中会收到
    child.on('message', (data) => {
        options.message(data); 
    });

    child.on('exit', (code) => {
        // 被调用 直接return
        if(invoked) {
            return;
        }

        invoked = true;
        options.exit(code);
    });

    child.on('error', (err) => {
        if(invoked){
            return;
        }

        invoked = true;
        options.error(err);
    })
}

// 七牛上传 https://developer.qiniu.com/kodo/1289/nodejs  下载文件部分
function qiniuUpload(options){
    const mac = new Qiniu.auth.digest.Mac(qiniu.keys.ak, qiniu.keys.sk),
            conf = new Qiniu.conf.Config(), // 在使用 Node.js SDK 中的FormUploader和ResumeUploader上传文件之前，必须要构建一个上传用的config
            client = new Qiniu.rs.BucketManager(mac, conf),
            key = nanoid() + options.ext; // 文件名

    return new Promise((resolve, reject) => {
        // 抓取资源到空间 将网络图片的url添加到对应空间，并赋值名字为key
        client.fetch(options.url, options.bucket, key, (error, ret, info) => {
            if(error){
                reject(error);
            }else{
                if(info.statusCode === 200){
                    // 上传成功获取文件名
                    resolve({key});
                    // console.log(ret);
                }else{
                    reject(info);
                    console.log(ret, info);
                }
            }
        });
    })
}

// 加密密码方法
function makeCrypto(str){
    const _md5 = crypto.createHash('md5'),
          content = `str=${str}&secret=${cryptoSecret}`;

    return _md5.update(content).digest('hex'); // 十六进制
}

// 清除空格
function trimSpace(str) {
    return str.replace(/\s+/g, '');
}

// 返回信息
function returnInfo(errorInfo, data) {
    if(data) {
        errorInfo.data = data;
    }

    return errorInfo;
}

module.exports = {
    startProcess,
    qiniuUpload,
    makeCrypto,
    trimSpace,
    returnInfo
}