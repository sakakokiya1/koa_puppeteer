var router = require('koa-router')(),
    crawlerController = require('../controllers/Crawler'),
    loginCheck = require('../middlewares/loginCheck');

// 添加前缀
router.prefix('/crawler');

// 轮播图数据爬取
router.get('/crawl_slider_data', loginCheck, crawlerController.crawlSliderData);

// js++ 信息爬取
router.get('/crawl_agency_info', loginCheck, crawlerController.crawlAgencyInfo);

// 获取课程信息
router.get('/crawl_recom_course', loginCheck, crawlerController.crawlRecomCourse);

// 获取课程信息
router.get('/crawl_collection', loginCheck, crawlerController.crawlCollection);

// 获取老师信息
router.get('/crawl_teacher', loginCheck, crawlerController.crawlTeacher);

// 获取学生信息
router.get('/crawl_student', loginCheck, crawlerController.crawlStudent);

// 获取课程分类
router.get('/crawl_course_tab', loginCheck, crawlerController.crawlCourseTab);

// 获取课程列表
router.get('/crawl_course_data', loginCheck, crawlerController.crawlCourseData);

// 获取关于我们
router.get('/crawl_aboutus', loginCheck, crawlerController.crawlAboutUs);


// 爬取数据管理
router.post('/crawl_action', loginCheck, crawlerController.crawlAction);

module.exports = router;
