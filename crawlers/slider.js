const crawler = require('../libs/crawler'),
      { crawler_cof } = require('../config/config');

crawler({
    // url: 'https://msiwei.ke.qq.com/#category=-1&tab=0',
    url: crawler_cof.url.main,
    callback(){
        const $ = window.$, 
              $item = $('.agency-big-banner-ul .agency-big-banner-li');

        // 声明接收数据的数组
        const data = [];

        $item.each((index, item) => {
            const $el = $(item),
                  $elLink = $el.find('.js-banner-btnqq'); // 获取a链接

            const dataItem = {
                cid: $elLink.attr('data-id'),
                href: $elLink.prop('href'), // 获取a链接的跳转地址
                title: $elLink.prop('title'), // 获取title属性
                imgUrl: $elLink.find('img').prop('src'), // 获取图片地址
                imgKey: '' // 上传之后的文件名
            };

            data.push(dataItem);
        });

        return data;
    }
})