const Crawler = require('../libs/crawler'),
      { crawler_cof } = require('../config/config');

Crawler({
    url: crawler_cof.url.teacher,
    callback(){
        const $ = window.$, 
              $item = $('.tea-wrap .tea');

        // 声明保存爬虫数据的数组
        const data = [];

        $item.each((index, item) => {
            const $el = $(item);

            const dataItem = {
                tid: index + 1,
                href: $el.find('.tea-face').prop('href'),
                teacherName: $el.find('.tea-face').prop('title'),
                teacherImg: 'https:' + $el.find('.tea-img').attr('lazy-src'),
                courseCount: parseInt($el.find('.tea-main-sub span').eq(0).text().replace(/[^0-9]/ig, '')),
                studentCount: parseInt($el.find('.tea-main-num').text().replace(/[^0-9]/ig, '')),
                intro: $el.find('.tea-main-cnt').prop('title'),
                teacherImgKey: ''
            };

            data.push(dataItem);
        });

        return data;
    }
})