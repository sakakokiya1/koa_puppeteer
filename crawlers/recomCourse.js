const Crawler = require('../libs/crawler'),
      { crawler_cof } = require('../config/config');

Crawler({
    url: crawler_cof.url.main,
    callback(){
        const $ = window.$, 
              $item = $('.spread-course-ul li'),
              mainTitle = $('.agency-spread-wrap h4').text();

        const data = [];

        $item.each((index, item) => {
            const $el = $(item),
                  $itemLk = $el.find('a');

            // 注意一定要有数据
            const dataItem = {
                cid: parseInt($el.attr('report-tdw').match(/\&(.+?)\&/)[1].split('=')[1]),
                href: $itemLk.prop('href'), // 点击图片的跳转链接
                mainTitle, // 大标题
                title: $itemLk.find('.spread-course-title').text().trim(), // 课程标题
                posterUrl: $itemLk.find('.spread-course-cover').prop('src'), // 课程图片
                // description: $el.find('.spread-course-des').text(), // 课程描述
                teacherImg: $el.find('.spread-course-face img').prop('src'), // 老师图片
                teacherName: $el.find('.spread-course-face span').eq(0).text(), // 老师姓名
                studentCount: parseInt($el.find('.spread-course-face span').eq(1).text().replace(/[^0-9]/ig, '')), // 学生人数
                price: Number($el.find('.spread-course-price').text().trim().slice(1)), // 课程价格
                posterKey: '', // 课程图片名称
                teacherImgKey: '', // 老师图片名称
            }

            data.push(dataItem);
        });


        return data;
    }
})