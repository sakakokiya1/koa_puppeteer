const Crawler = require('../libs/crawler'),
      { crawler_cof } = require('../config/config');

Crawler({
    url: crawler_cof.url.course,
    callback(){
        const $ = window.$, 
              $item = $('.course-tab-filter li');

        const data = [];

        $item.each((index, item) => {
            const $el = $(item),
                  $itemLk = $el.find('.course-tab-filter-item'),
                  title = $itemLk.text().replace('促', '');

            // 不爬取全部
            if(title !== '全部'){
                const dataItem = {
                    cid: index,
                    title: $itemLk.text().replace('促', '')
                };
    
                data.push(dataItem);
            }
        });

        return data;
    }
})