const CourseTabModel = require('../db/models/courseTab');

class CourseTabService {
    async addCourseTab(data){
        const cid = data.cid;

        try{
            const res = await CourseTabModel.findOne({
                where: { cid }
            });
    
            if(res){
                return await CourseTabModel.update(data, {
                    where: { cid }
                })
            } else {
                return await CourseTabModel.create(data);
            }
        } catch(err){
            console.log('数据库操作：' + err);
        }
    }

    // 获取全部课程分类
    async getCourseFieldData() {
        return await CourseTabModel.findAll({
            attributes: {
                exclude: ['cid']
            }
        });
    }

}

module.exports = new CourseTabService();