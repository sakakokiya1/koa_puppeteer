const AboutUsModel = require('../db/models/aboutus');

class AboutUsService {
    async addAboutus(data){
        const aid = data.aid;

        const res = await AboutUsModel.findOne({
            where: { aid }
        });

        if(res){
            return await AboutUsModel.update(data, {
                where: { aid }
            });
        } else {
            return await AboutUsModel.create(data);
        }
    }
}

module.exports = new AboutUsService();