// 获取表模型
const RecomCourseModel = require('../db/models/recomCourse');

class RecomCourseService {
    // 添加数据
    async addRecomCourse(data){
        const cid = data.cid;

        const res = await RecomCourseModel.findOne({
            where: { cid }
        });
 
        if(res){
            return await RecomCourseModel.update(data, {
                where: { cid }
            })
        } else {
            return await RecomCourseModel.create(data);
        }
    }

    // 获取全部推荐课程
    async getRecomCourseData() {
        return await RecomCourseModel.findAll({
            attributes: {
                exclude: ['mainTitle', 'posterUrl', 'description', 'teacherImg']
            }
        });
    }

    // 修改推荐课程状态
    async changeRecomCourseStatus(id, status) {
        const res = await RecomCourseModel.update({ status }, {
            where: { cid: id }
        });

        return res[0];
    }
}

module.exports = new RecomCourseService();