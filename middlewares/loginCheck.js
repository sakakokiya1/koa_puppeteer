const { returnInfo } = require('../libs/utils'),
      { LOGIN } = require('../config/error_config');

// 登录判断中间件
module.exports = async (ctx, next) => {
    // 存在用户session信息执行下一个中间件
    if(ctx.session.userInfo){
        await next();
        return;
    }

    // 未登录
    ctx.body = returnInfo(LOGIN.NOT_LOGIN_STATUS);
}