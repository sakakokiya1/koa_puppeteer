module.exports = {
	app: [
    {
    	name: 'api',
    	script: 'app.js',
    	env: {
    		COMMON_VARIABLE: 'true'
    	},
    	env_production: {
    		NODE_ENV: 'production'
    	}
    }
	],

	deploy: {
		production: {
			user: 'root',
			host: '101.43.223.114',
			ref: 'origin/master',
			repo: 'https://gaotengjun:15963936348gtj@gitee.com/gaotengjun/koa_puppeteer.git',
			path: '/www/jspp_api/production',
			'pre-deploy': 'git fetch --all',
			'post-deploy': 'npm install && npm run prd && pm2 startOrRestart deploy.config.js --env production'
		}
	}
}