const { REDIS_CONF } = require('./db_config'),
      { isPrd } = require('./env_config')

module.exports = {
    // 七牛账户配置
    qiniu: {
        keys: {
            // 秘钥管理中的ak sk
            ak: 'Jkt4cUXADFOlFRFrK3AUGLsP_gKEmIeNcbzVQSxj',
            sk: '7gtu3n6vekAHMbBrCrbeRRFOatpD5b2By46OpdHz',
        }, 
        bucket: { // 可能存在多个空间
            tximg: {
                bucket_name: 'gtj-txclass-img', // 空间名
                domain: 'http://tximg.gaotengjun.fun/', // 加速域名
            }
        }
    },
    crawler_cof: {
        url: {
            main: 'https://msiwei.ke.qq.com/#tab=0&category=-1', // 主页
            course: 'https://msiwei.ke.qq.com/#tab=1&category=-1', // 课程
            teacher: 'https://msiwei.ke.qq.com/#tab=2&category=-1', // 老师
            aboutus: 'https://msiwei.ke.qq.com/#tab=3&category=-1', // 关于我们
        }
    },
    sessionInfo: {
        keys: ['a1!s2@d3#f4$_+g5%h6^'], // 加密cookie的key
        name: 'txclass.sid', // cookie name 一般以项目名加点sid
        prefix: 'txclass.sess' // redis KEY 前缀
    },
    cookieInfo: {
        path: '/',
        httpOnly: true,
        maxAge: 24 * 60 * 60 * 1000
    },
    redisInfo: {
        // all: `${REDIS_CONF[1]}:${REDIS_CONF[0]}` // 127.0.0.1:6379
        host: '127.0.0.1',
        port: 6479
    },
    adminInfo: { // 默认用户名密码
        username: 'admin',
        password: 'admin'
    },
    cryptoSecret: 'SLFDasj@#$kddfj354&%', // 加密秘钥 随便写
    corsOrigin: isPrd ? 'http://admin.gaotengjun.fun' : 'http://localhost:3000', // 可跨域访问地址
}