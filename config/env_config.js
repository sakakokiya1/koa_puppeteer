// 获取当前环境
const ENV = process.env.NODE_ENV;
console.log(ENV);
module.exports = {
    isDev: ENV === 'dev',
    isPrd: ENV === 'production',
}